import { Resolver, Mutation, Query, InputType, Arg, Field } from "type-graphql";
import Category from "./Category";
import CategorySchema from "../../model/CategorySchema";

@InputType()
class CategoryInput {
	@Field()
	description: String;
	@Field()
	name: String;
}

@Resolver(Category)
class CategoryResolver {
	@Query(() => [Category])
	async getCategories() {
		const categories = await CategorySchema.find();
		return categories;
	}

	@Mutation(() => Category)
	async createCategory(@Arg("categoryInput") categoryInput: CategoryInput) {
		const category = await CategorySchema.create(categoryInput);
		return category;
	}
}

export default CategoryResolver;
