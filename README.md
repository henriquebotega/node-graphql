https://typegraphql.com/

http://localhost:4100/graphql

query {
getCategories {
\_id,
description
}
}

query {
getVideos {
\_id,
description
}
}

mutation {
createCategory(categoryInput: {
name: "titulo 1",
description: "descricao 1"
}) {
\_id,
description
}
}

mutation {
createVideo(videoInput: {
name: "video 1",
description: "video descricao 1",
category: "603ecbfcee5f221cd85e01fb"
}) {
\_id,
description
}
}
