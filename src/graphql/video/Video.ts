import { ObjectType, Field } from "type-graphql";

@ObjectType()
class Video {
	@Field()
	_id: String;
	@Field()
	description: String;
	@Field()
	name: String;
	@Field()
	category: String;
}

export default Video;
