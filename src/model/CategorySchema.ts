import mongoose from "mongoose";

const Schema = new mongoose.Schema({
	description: String,
	name: String,
});

export default mongoose.model("Categories", Schema);
